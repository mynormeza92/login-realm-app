import React from "react";
import { Button, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {signOut} from "../store/LoginActions";
function Logout(props) {
  const navigation = useNavigation();
  console.log(props)

  return (
    <Button
      title="Log Out"
      onPress={() => {
        Alert.alert("Log Out", null, [
          {
            text: "Yes, Log Out",
            style: "destructive",
            onPress: () => {
              props.signOut()
              navigation.popToTop();
            },
          },
          { text: "Cancel", style: "cancel" },
        ]);
      }}
    />
  );
}

const mapStateToProps = (state) => {
  const { loggedJWT } = state
  return { loggedJWT }
};


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    signOut,
  }, dispatch)
);

export default connect(mapStateToProps,mapDispatchToProps)(Logout);

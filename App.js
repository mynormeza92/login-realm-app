import React from "react";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import LoginView from "./views/LoginView";
import { HomeView } from "./views/HomeView";

import Logout from "./components/Logout";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import loginReducer from "./store/LoginReducer";

const Stack = createStackNavigator();
const store = createStore(loginReducer);
const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login View"
            component={LoginView}
            options={{ title: "Task Tracker" }}
          />
          <Stack.Screen
            name="Home View"
            component={HomeView}
            title="Home"
            headerBackTitle="log out"
            options={{
              headerRight: function Header() {
                return <Logout />;
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;

import React, { useEffect, useState } from "react";
import { View, Text, TextInput, Button, Alert } from "react-native";
import styles from "../stylesheet";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from "axios";
import {signIn} from "../store/LoginActions";


function LoginView(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { loggedJWT, navigation, signIn } = props

  useEffect(() => {
    // If there is a user logged in, go to the Projects page.
    if (loggedJWT.currentToken != null) {
      navigation.navigate("Home View");
    }
  }, [loggedJWT.currentToken]);

  // The onPressSignIn method calls AuthProvider.signIn with the
  // email/password in state.
  const onPressSignIn = async () => {
    console.log("Press sign in");
    try {
      const response =  await axios.get(`https://raw.githubusercontent.com/mynormeza/mock/master/${username + password}.json`);
      const authResult = response.data;
      signIn(authResult.token);
    } catch (error) {
      console.log({error})
      Alert.alert(`Failed to sign in: ${error.message}`);
    }
  };

  return (
    <View  style={{flex: 1, flexDirection: 'column',  justifyContent: 'center', alignItems: 'stretch'}}>
      <Text>Signin:</Text>
      <View style={styles.inputContainer}>
        <TextInput
          onChangeText={setUsername}
          value={username}
          placeholder="username"
          style={styles.inputStyle}
          autoCapitalize="none"
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          onChangeText={(text) => setPassword(text)}
          value={password}
          placeholder="password"
          style={styles.inputStyle}
          secureTextEntry
        />
      </View>
      <View style={{ flexShrink: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
        <Button  onPress={onPressSignIn} title="Sign In" />
      </View>
      
    </View>
  );
}




const mapStateToProps = (state) => {
  const { loggedJWT } = state
  return { loggedJWT }
};


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    signIn,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);

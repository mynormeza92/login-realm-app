import React from "react";
import { Text, View } from 'react-native';

export function HomeView() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>
        You are in! 🎉
      </Text>
    </View>
  );
}

import { combineReducers } from 'redux';

const INITIAL_STATE = {
    currentToken: null
};

const loginReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SING_IN' :
            if (action.payload === 'wkahjkdhfkjhasdasdf') {
                return { currentToken: action.payload }
            }
            return state;
        case 'SING_OUT':
            return {currentToken: null};
        default:
            return state
    }
};

export default combineReducers({
    loggedJWT: loginReducer
});
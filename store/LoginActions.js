export const signIn = JWTtoken => (
    {
        type: 'SING_IN',
        payload: JWTtoken,
    }
);

export const signOut = () => (
    {
        type: 'SING_OUT',
        payload: null
    }
);